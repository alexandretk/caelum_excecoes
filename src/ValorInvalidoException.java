
public class ValorInvalidoException extends RuntimeException{
   
	public ValorInvalidoException() {
		super("Valor Invalido");
	}

    //Construtor com mensagem
    public ValorInvalidoException(String message)
    {
       super(message);
    }
    
    //Contrutor com valor
    public ValorInvalidoException(double valor) {
    	super("O valor de "+ valor + " e invalido. Digite um numero positivo!");
    }

}
